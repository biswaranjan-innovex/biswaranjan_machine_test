﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Test.Data;

namespace Test.Models
{
    public class ProductViewModels
    {
        public int ProductId { get; set; }
        
      
        [Required]
        [Display(Name = "Category Name")]
        [MaxLength(50)]
        public string ProductName { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public ProductViewModels() { }

        public ProductViewModels(int ProductId)
        {
            TestEntities dbEntities = new TestEntities();
            Product product = dbEntities.Products.Where(item => item.ProductId == ProductId).FirstOrDefault();

            SetProperties(product);
        }

        public ProductViewModels(Product product)
        {
            SetProperties(product);
        }


        private void SetProperties(Product product)
        {
            this.ProductId = product.ProductId;
            this.ProductName = product.ProductName;
            this.CategoryId = product.CategoryId;
            this.CategoryName = product.Category.CategoryName;

        }

        public static List<ProductViewModels> ModelsToViewModels(List<Product> product)
        {
            List<ProductViewModels> productVMs = new List<ProductViewModels> { };

            if (product != null)
            {
                foreach (Product item in product)
                {
                    productVMs.Add(new ProductViewModels(item));
                }
            }
            return productVMs;
        }
    }
}
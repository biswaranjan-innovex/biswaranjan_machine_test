﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Test.Data;

namespace Test.Models
{
    public class CategoryViewModel
    {

        public int CategoryId { get; set; }

        [Required]
        [Display (Name = "Category Name")]
        [MaxLength(50)]
        public string CategoryName { get; set; }    

        public CategoryViewModel() { }

        public CategoryViewModel(int CategoryId)
        {
            TestEntities dbEntities = new TestEntities();
            Category category = dbEntities.Categories.Where(item => item.CategoryId == CategoryId).FirstOrDefault();

            SetProperties(category);
        }

        public CategoryViewModel(Category category)
        {
            SetProperties(category);
        }


        private void SetProperties(Category category)
        {
            this.CategoryId = category.CategoryId;
            this.CategoryName = category.CategoryName;

        }

        public static List<CategoryViewModel> ModelsToViewModels(List<Category> Category)
        {
            List<CategoryViewModel> CategoryVMs = new List<CategoryViewModel> { };

            if (Category != null)
            {
                foreach (Category item in Category)
                {
                    CategoryVMs.Add(new CategoryViewModel(item));
                }
            }
            return CategoryVMs;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class CategoryListViewModels
    {
        public int maxrows { get; set; }

        public List<CategoryViewModel> categoryVms { get; set; }
    }
}
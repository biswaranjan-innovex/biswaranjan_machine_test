﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class ProductListViewModels
    {
        public int maxrows { get; set; }

        public List<ProductViewModels> productVms { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index(int? Page)
        {
            TestEntities dbEntities = new TestEntities();
            List<Category> categoryList = new List<Category>();
            List<Category> categoryTotalList = dbEntities.Categories.ToList();
            int count = categoryTotalList.Count();
            int pagesize = 10;
            int maxrows = 0;
            maxrows = count / pagesize;
            if (count % pagesize != 0)
            {
                maxrows = maxrows + 1;
            }
            if (Page != 0)
            {
               

                int currentPage = Page ?? 0;
                currentPage = currentPage + 1;
                int diff = (currentPage * pagesize) - count;
                categoryList = dbEntities.Categories.Take(currentPage * pagesize).ToList();

                if (diff > 0)
                {
                    categoryList = categoryList.OrderByDescending(item => item.CategoryId).Take(count % pagesize).ToList();
                }
                else
                {
                    categoryList = categoryList.OrderByDescending(item => item.CategoryId).Take(pagesize).ToList();
                }
            }
            else
            {
                categoryList = categoryTotalList.Take(pagesize).ToList();
            }
            categoryList = categoryList.OrderBy(item => item.CategoryId).ToList();
            List<CategoryViewModel> categoryVmList = CategoryViewModel.ModelsToViewModels(categoryList);
            CategoryListViewModels model = new CategoryListViewModels();
            model.categoryVms = categoryVmList;
            model.maxrows = maxrows;
            return View(model);
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(CategoryViewModel categoryVM)
        {
            try
            {
                // TODO: Add insert logic here

                TestEntities dbEntities = new TestEntities();
                if (dbEntities.Categories.Any(item => (item.CategoryName.ToLower() == categoryVM.CategoryName.ToLower())))
                {
                    string error = "This Category Name is already exist";
                    ModelState.AddModelError("", error);
                    return View();
                }
                else
                {
                    Category category = new Category();
                    category.CategoryName = categoryVM.CategoryName;
                    dbEntities.Categories.Add(category);
                    dbEntities.SaveChanges();
                    return RedirectToAction("Index");

                }
             
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            CategoryViewModel model = new CategoryViewModel(id);
            return View(model);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CategoryViewModel categoryVM)
        {
            try
            {
                // TODO: Add update logic here
                TestEntities dbEntities = new TestEntities();
                if (dbEntities.Categories.Any(item => item.CategoryId != id && (item.CategoryName.ToLower() == categoryVM.CategoryName.ToLower())))
                {
                    string error = "This Category Name is already exist";
                    ModelState.AddModelError("", error);
                    return View();
                }
                else
                {
                    Category category = dbEntities.Categories.Where(item => item.CategoryId == id).FirstOrDefault();
                    category.CategoryName = categoryVM.CategoryName;
                    dbEntities.SaveChanges();
                    return RedirectToAction("Index");
                }
               
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            CategoryViewModel model = new CategoryViewModel(id);
            return View(model);
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                TestEntities dbEntities = new TestEntities();
                Category category = dbEntities.Categories.Where(item => item.CategoryId == id).FirstOrDefault();
                dbEntities.Categories.Remove(category);
                dbEntities.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

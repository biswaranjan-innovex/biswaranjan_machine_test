﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(int? Page)
        {
            TestEntities dbEntities = new TestEntities();
            List<Product> productList = new List<Product>();
            List<Product> productTotalList = dbEntities.Products.ToList();
            int count = productTotalList.Count();
            int pagesize = 10;
            int maxrows = 0;
            maxrows = count / pagesize;

            if (count % pagesize != 0)
            {
                maxrows = maxrows + 1;
            }

            if (Page != 0)
            {
                int currentPage = Page ?? 0;
                currentPage = currentPage + 1;
                int diff = (currentPage * pagesize) - count;
                 productList = dbEntities.Products.Take(currentPage * pagesize).ToList();
                if (diff > 0)
                {
                    productList = productList.OrderByDescending(item => item.ProductId).Take(count % pagesize).ToList();
                }
                else
                {
                    productList = productList.OrderByDescending(item => item.ProductId).Take(pagesize).ToList();
                }
                productList = productList.OrderByDescending(item => item.ProductId).Take(pagesize).ToList();
            }
           else
            {
                productList = productTotalList.Take(pagesize).ToList();
            }
            productList = productList.OrderBy(item => item.ProductId).ToList();
            List<ProductViewModels> productVmList = ProductViewModels.ModelsToViewModels(productList);
            ProductListViewModels model = new ProductListViewModels();
            model.productVms = productVmList;
            model.maxrows = maxrows;
            return View(model);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            TestEntities dbEntities = new TestEntities();
            List<Category> categoryList = dbEntities.Categories.ToList();
            ViewBag.categoryList = new SelectList(categoryList, "CategoryId", "CategoryName");
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(ProductViewModels productVM)
        {
            try
            {
                // TODO: Add insert logic here
                TestEntities dbEntities = new TestEntities();
                if (dbEntities.Products.Any(item => item.ProductName.ToLower() == productVM.ProductName.ToLower()))
                {
                    string error = "This Product Name is already exist";
                    ModelState.AddModelError("", error);

                    List<Category> categoryList = dbEntities.Categories.ToList();
                    ViewBag.categoryList = new SelectList(categoryList, "CategoryId", "CategoryName");

                    return View();

                }
                else
                {
                    Product product = new Product();
                    product.ProductName = productVM.ProductName;
                    product.CategoryId = productVM.CategoryId;
                    dbEntities.Products.Add(product);
                    dbEntities.SaveChanges();
                    return RedirectToAction("Index");
                }
               
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            TestEntities dbEntities = new TestEntities();
            
            List<Category> categoryList = dbEntities.Categories.ToList();
            ViewBag.categoryList = new SelectList(categoryList, "CategoryId", "CategoryName");
            ProductViewModels model = new ProductViewModels(id);
            return View(model);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModels productVM)
        {
            try
            {
                // TODO: Add update logic here
                TestEntities dbEntities = new TestEntities();
                if (dbEntities.Products.Any(item => item.ProductId != id && (item.ProductName.ToLower() == productVM.ProductName.ToLower())))
                {
                    string error = "This Product Name is already exist";
                    ModelState.AddModelError("", error);

                    List<Category> categoryList = dbEntities.Categories.ToList();
                    ViewBag.categoryList = new SelectList(categoryList, "CategoryId", "CategoryName");
                    ProductViewModels model = new ProductViewModels(id);
                    return View(model);

                }
                else
                {
                    Product product = dbEntities.Products.Where(item => item.ProductId == id).FirstOrDefault();
                    product.ProductName = productVM.ProductName;
                    product.CategoryId = productVM.CategoryId;
                    dbEntities.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            ProductViewModels model = new ProductViewModels(id);
            return View(model);
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                TestEntities dbEntities = new TestEntities();
                Product product = dbEntities.Products.Where(item => item.ProductId == id).FirstOrDefault();              
                dbEntities.Products.Remove(product);
                dbEntities.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
